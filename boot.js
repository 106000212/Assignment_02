var bootState ={
    preload:function(){
        game.load.image('bullet', 'ASS/bullet.png');
        game.load.image('bullet2', 'ASS/bullet2.png');
        game.load.image('bossbullet', 'ASS/bossbullet.png');
        game.load.image('enemyBullet', 'ASS/enemy-bullet.png');
        game.load.image('aimBullet', 'ASS/aim-bullet.png');
        game.load.image('live', 'ASS/live.png');
        game.load.image('burstb', 'ASS/burstb.png');
        game.load.spritesheet('invader', 'ASS/invader32x32x4.png', 32, 32);
        game.load.spritesheet('boss', 'ASS/Boss.png', 250, 230);
        game.load.spritesheet('powerring', 'ASS/Power_Ring.png', 50, 50);
        game.load.image('pixel', 'ASS/pixel.png');
        game.load.image('helper', 'ASS/player.png');
        game.load.image('progressBar','ASS/progressBar.png');
        game.load.spritesheet('ship', 'ASS/player1.png', 68 ,103);
        game.load.spritesheet('kaboom', 'ASS/explode.png', 128, 128);
        game.load.image('starfield', 'ASS/starfield.png');
        game.load.image('background', 'ASS/background2.jpg');
        game.load.image('cbackground', 'ASS/background3.jpg');
        game.load.audio('backmusic',['ASS/backmusic.ogg','ASS/backmusic.mp3']);
        game.load.audio('bbackmusic',['ASS/bbackmusic.ogg','ASS/bbackmusic.mp3']);
        game.load.audio('sshoot',['ASS/sshoot.ogg','ASS/sshoot.mp3']);
        game.load.audio('sexplosion',['ASS/sexplosion.ogg','ASS/sexplosion.mp3']);
    },
    create:function(){
        game.stage.backgroundColor = 'black';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        game.state.start('load');
    },
};