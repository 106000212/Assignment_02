# Software Studio 2019 Spring Assignment2-Raiden
## Notice

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucifymechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|N|


## Bonus Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi player game|20%|N|
|Bullet automatic aiming|5%|Y|
|Little helper|5%|Y|
|Boss|5%|Y|
|enemybullet|
## Website Detail Description
我在Basic ComponentsDescription會有仔細講解各頁面和功能
# 作品網址：[https://106000212.gitlab.io/Assignment_02]
# 操作說明:
 用上下左右鍵移動player，空白鍵發射子彈，吃到ring集到5格burst計量後長按X鍵發動必殺技，音量控制按A或D鍵可以調高或低音量，按Q鍵可以暫停遊戲，|level1吃到8個，level2吃到10個ring時可發出追蹤敵人子彈，level1吃到4個ring，level2吃到6個ring時player旁會有helper會自動發射攻擊

# Basic Components Description : 
1. |Complete game process|在進menu前有讀取畫面->進入menu後按空白鍵進入操作說明->進入操作說明後按空白鍵進入遊戲->遊戲畫面level1->成功後進入level2，失敗進入menu(滑鼠點擊操控)->遊戲畫面level2->成功會由勝利字幕並回到menu，失敗有失敗字幕並回到menu(滑鼠點擊操控)
2. |Basic rules|飛機可用上下左右鍵移動，空白鍵發射子彈，敵人和boss有各自的移動方式和子彈，被敵人和boss打到右上方血量條會減少，歸零時即為遊戲失敗，擊敗boss及該level增加，地圖背景會移動
3. |Jucifymechanisms|
level:有level1和level2兩種難度，level1較為簡單，成功通關後進入level2且敵人 boss 子彈，移動會變很多很快，player移動速度會減少，相對擊敗敵人得到分數也會變多
skill:右下角有burst計量表，吃到ring會增加burst計量，最多集到5格不能再增加後長按X鍵發動必殺技，使用後burst計量表歸零
4. |Animations|player 敵人 boss都有各自動畫 被子彈擊中會有爆炸特效
5. |Particle Systems|子彈打到boss會有Particle效果，打到敵人則沒有而是爆炸特效
6. |Sound effects|遊戲畫面有bgm，射出子彈和爆炸有音效，按鍵可以調整音量，按A鍵可以調小音量，按D鍵可以調大音量
7. |UI|右上角有血量，有五格血，被打到會減一血，歸零即失敗，右下有必殺技量表，以吃到ring數量會增加burst計量，左上有分數，打死敵人 BOSS或 吃到ring會加分，左下會有level1 2等關卡難度顯示，音量控制按A或D鍵可以調高或低音量，按Q鍵可以暫停遊戲


# Bonus Functions Description : 
1. |Bullet automatic aiming|level1吃到8個，level2吃到10個ring時可發出追蹤敵人子彈
2. |Little helper|level1吃到4個ring，level2吃到6個ring時player旁會有helper會自動發射攻擊
3. |Boss|level 50滴血，level2 250滴血，boss有屬於自己的攻擊和移動，有特殊子彈，死亡時也有特效，Particle Systems在擊中boss會觸發
4. |enemybullet|敵人的子彈會追蹤玩家，所以難度很高，但速度沒Bullet automatic aiming那麼快
