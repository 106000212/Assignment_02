var menuState ={
    preload:function(){
        
    },
    create:function(){
        game.add.image(-700,-300,'background1');
        var nameLabel = game.add.text(game.width/2,80,'Raiden',{font:'50px Arial',fill:'#ffffff'});

        nameLabel.anchor.setTo(0.5,0.5);
        var startLabel = game.add.text(game.width/2,380,'please press SPACEBAR key to start',{font:'25px Arial',fill:'#ffffff'});
        startLabel.anchor.setTo(0.5,0.5);
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        upKey.onDown.add(this.start,this);
    },
    start:function(){
        game.state.start('control');
    },
};