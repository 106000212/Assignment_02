var MainState2={

    preload: function(){      
    },
    
  



    create: function(){

        game.physics.startSystem(Phaser.Physics.ARCADE);
        //  The scrolling starfield background
        this.starfield = game.add.tileSprite(0, 0, 800, 600, 'background');
        game.renderer.renderSession.roundPixels = true;
        //  The ship!
        this.player = game.add.sprite(400, 500, 'ship');
        this.player.anchor.setTo(0.5, 0.5);
        game.physics.enable(this.player, Phaser.Physics.ARCADE);
        

        this.player.animations.add('leftfly',[4,3,2,1,0],8,false);
        this.player.animations.add('rightfly',[6,7,8,9,10],8,false);
        this.player.frame = 5;
        ///////////
        game.paused = false;
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        upKey.onDown.add(this.unpause, this);
       ////////////
        
       this.helper = game.add.sprite(-200, 0, 'helper');
       this.helper.anchor.setTo(0.5, 0.5);
       game.physics.enable(this.helper, Phaser.Physics.ARCADE);
        
        ///boss
        this.boss = game.add.sprite(300, -4000, 'boss');
        this.boss.anchor.setTo(0.5, 0.5);
        game.physics.enable(this.boss, Phaser.Physics.ARCADE);
        this.boss.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        this.boss.play('fly');
        this.boss.body.velocity.y = 45;
        var tween1 = game.add.tween(this.boss).to( { x: 500 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        this.bosshitnum=0;
        this.bosskill=false;
        ///boss shoot
        this.bossbulletTime = 0;
        this.bossbullets = game.add.group();
        this.bossbullets.enableBody = true;
        this.bossbullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bossbullets.createMultiple(30, 'bossbullet');
        this.bossbullets.setAll('anchor.x', 0.5);
        this.bossbullets.setAll('anchor.y', 1);
        this.bossbullets.setAll('outOfBoundsKill', true);
        this.bossbullets.setAll('checkWorldBounds', true);
        


        bulletTime = 0;
        //  Our bullet group
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(50, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);


        ////
        hbulletTime=0;
        




        this.enhancednum=0;
        aimbulletTime = 0;
        //  Our aimbullet group
        this.aimbullets = game.add.group();
        this.aimbullets.enableBody = true;
        this.aimbullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.aimbullets.createMultiple(50, 'aimBullet');
        this.aimbullets.setAll('anchor.x', 0.5);
        this.aimbullets.setAll('anchor.y', 1);
        this.aimbullets.setAll('outOfBoundsKill', true);
        this.aimbullets.setAll('checkWorldBounds', true);


         /// Particle
         this.emitter = game.add.emitter(this.boss.x, this.boss.y+10, 100);
         this.emitter.makeParticles('pixel');
         this.emitter.setYSpeed(-150, 150);
         this.emitter.setXSpeed(-150, 150);
         this.emitter.setScale(1.5, 0, 1.5, 0, 1200);
         this.emitter.gravity = 500;

        bulletTimeX = 0;
        //  Our bullet group//skillx
        this.Xbullets = game.add.group();
        this.Xbullets.enableBody = true;
        this.Xbullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.Xbullets.createMultiple(100, 'bullet2');
        this.Xbullets.setAll('anchor.x', 0.5);
        this.Xbullets.setAll('anchor.y', 1.5);
        this.Xbullets.setAll('outOfBoundsKill', true);
        this.Xbullets.setAll('checkWorldBounds', true);


        this.firingTimer = 0;
        // The enemy's bullets
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(30, 'enemyBullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);


        //  The baddies!
        this.aliens = game.add.group();
        this.aliens.enableBody = true;
        this.aliens.physicsBodyType = Phaser.Physics.ARCADE;
        this.createAliens();




        //the eat object
        this.PowerRings = game.add.group();
        this.PowerRings.enableBody = true;
        this.PowerRings.physicsBodyType = Phaser.Physics.ARCADE;
        
        
        this.createPowerRings();



        //  The score
        this.scoreString = 'Score : ';
        this.score=0;
        this.scoreText = game.add.text(10, 10, this.scoreString + this.score, { font: '34px Arial', fill: '#fff' });
    
        //  Lives
        this.lives = game.add.group();
        game.add.text(game.world.width - 250, 10, 'Lives : ', { font: '28px Arial', fill: '#fff' });
        
        //burst power
        this.bursttime = 0;
        this.powers = game.add.group();
        game.add.text(game.world.width - 200, 550, 'Burst : ', { font: '28px Arial', fill: 'yellow' });
        //  Text

        this.stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '42px Arial', fill: '#fff' });
        this.stateText.anchor.setTo(0.5, 0.5);
        this.stateText.visible = false;
        ////live
        for (var i = 0; i < 5; i++) {
            var live = this.lives.create(game.world.width - 150 + (30 * i), 30, 'live');
            live.anchor.setTo(0.5, 0.5);
            live.alpha = 0.4;
        }

        game.add.text(0, 550, 'level1 ', { font: '20px Arial', fill: 'yellow' });
        //power bar
        powernum=0;

        
        //  An explosion pool
        this.explosions = game.add.group();
        this.explosions.createMultiple(30, 'kaboom');
        this.explosions.forEach(this.setupInvader, this);


        //sound
        this.backmusic=game.add.audio('bbackmusic');
        this.sshoot=game.add.audio('sshoot');
        this.sexplosion=game.add.audio('sexplosion');
        
        this.backmusic.play();
        this.backmusic.volume=0.5;//////
        this.backmusic.loop=true;
        //this.shoot1.play();
        //this.shoot1.volume=0;
        //this.shoot1.loop=true;

        //  And some controls to play the game with

        this.cursors = game.input.keyboard.createCursorKeys();
        this.fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.XButton = game.input.keyboard.addKey(Phaser.Keyboard.X);
        this.pauseButton = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        this.VaButton = game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.VdButton = game.input.keyboard.addKey(Phaser.Keyboard.D);
        this.RButton = game.input.keyboard.addKey(Phaser.Keyboard.R);

        
        
        

    },
    update: function () {
        

        //  Scroll the background
       
        this.starfield.tilePosition.y += 1;
        //this.boss.body.velocity.y = 50;
        
        if (this.player.alive){

            //  Reset the player, then check for movement keys
            this.player.body.velocity.setTo(0, 0);
            if (this.cursors.left.isDown){
                this.player.body.velocity.x = -150;
                if(this.player.frame != 0){
                    this.player.animations.play('leftfly');
                }else{
                    this.player.frame = 0;
                }

            }else if (this.cursors.right.isDown){
                this.player.body.velocity.x = 150;
                
                if(this.player.frame != 10){
                    this.player.animations.play('rightfly');
                }else{
                    this.player.frame = 10;
                }

            }
            if(this.cursors.up.isDown){
                this.player.body.velocity.y = -150;
                
                
            }
            if(this.cursors.down.isDown){
                this.player.body.velocity.y = 150;
                
                
            }
            if(!this.cursors.right.isDown&&!this.cursors.left.isDown){
                this.player.frame = 5;
            }
            

            //  Firing?
            if (this.fireButton.isDown){
                this.fireBullet();
                this.sshoot.play();
                
                
            }
            if (this.XButton.isDown&&this.powers.countLiving()==5){//bursttb will kill after burst
                this.fireX();
                this.sshoot.play();
                game.time.events.add(3000,function(){this.powers.callAll('kill');powernum=0;;},this)
               // powernum=0;
                    
                
            }

            //////pause button
            
            if (this.pauseButton.isDown&&game.paused == false){
                game.paused = true;
                
            }
            
           // if (this.pauseButton.isDown&&game.paused == true){
            //    game.paused = false;
                
           // }//目前無法回復
            
           //////////SOUND VOLUME
           if (this.VaButton.isDown&&this.backmusic.volume>=0){
                this.backmusic.volume=0.25 ;
               
            }
            if (this.VdButton.isDown&&this.backmusic.volume>=0&&this.backmusic.volume<=1){
                this.backmusic.volume=0.85 ;
                
            }

            if (this.VaButton.isDown&&this.sshoot.volume>=0){
                this.sshoot.volume=0.25;
            
            }
            if (this.VdButton.isDown&&this.sshoot.volume>=0&&this.sshoot.volume<=1){
                
                this.sshoot.volume=0.85;
                
            }

            if (game.time.now > this.firingTimer){
                this.enemyFires();
            }
            if (game.time.now > this.bossbulletTime&&this.boss.y>=100){
                this.bossFires();
            }
            if(this.boss.y>=100){
                this.boss.body.velocity.y = 0;
            }
            if(this.RButton.isDown){
                this.restart();
            }
            //enhanced item
            if(this.enhancednum>=10&&this.fireButton.isDown){
                this.getaimBullet();
            }
            if(this.enhancednum>=6){
                this.gethelper();
                this.helperfireBullet();
            }

            //  Run collision
            game.physics.arcade.overlap(this.bullets, this.aliens, this.collisionHandler, null, this);
            game.physics.arcade.overlap(this.aimbullets, this.aliens, this.collisionHandlerforaim, null, this);
            game.physics.arcade.overlap(this.Xbullets, this.aliens, this.collisionHandlerforX, null, this);
            game.physics.arcade.overlap(this.enemyBullets,this.player, this.enemyHitsPlayer, null, this);
            game.physics.arcade.overlap(this.PowerRings,this.player, this.HitsCoin, null, this);
            game.physics.arcade.overlap(this.bullets,this.boss, this.hitboss1, null, this);
            game.physics.arcade.overlap(this.Xbullets,this.boss, this.hitboss2, null, this);
            game.physics.arcade.overlap(this.aimbullets,this.boss, this.hitboss3, null, this);
            game.physics.arcade.overlap(this.bossbullets,this.player, this.BossHitsPlayer, null, this);
            
        }
    },

   /* unpause: function(event){
        if (this.pauseButton.isDown&&game.paused == true){
            //game.time.events.add(50,function(){game.paused = false;},this)
            game.paused= false;
            
        }
    },*/
    HitsCoin: function(player,PowerRing){
        PowerRing.kill();
        //player.kill();
        this.score += 50;
        this.scoreText.text = this.scoreString + this.score;
        powernum+=1;
        this.enhancednum+=1;
        if(powernum<=5){
            var power = this.powers.create(game.world.width - 100 + (15 * powernum), 560, 'burstb');
            power.anchor.setTo(0.5, 0.5);
            power.alpha = 0.4;
        }
    },
    createPowerRings: function(){//coin create move
       
        for (var x = 0; x < 17; x++){
            var PowerRing = this.PowerRings.create(100, -500+x*(-400), 'powerring');
            
            PowerRing.anchor.setTo(0.5, 0.5);
            PowerRing.animations.add('rolling',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],8,true);
            PowerRing.play('rolling');
            PowerRing.body.velocity.y = 80;
            //var tween = game.add.tween(this.PowerRings).to( { x: 500 }, 3000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        }
        for (var x = 0; x < 6; x++){
            var PowerRing2 = this.PowerRings.create(100, x*(-400), 'powerring');
            
            PowerRing2.anchor.setTo(0.5, 0.5);
            PowerRing2.animations.add('rolling',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],8,true);
            PowerRing2.play('rolling');
            PowerRing2.body.velocity.y = 20;
            
        }
        var tween = game.add.tween(this.PowerRings).to( { x: 500 }, 3000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        this.PowerRings.x = 100;
    },
    createAliens: function(){//五波小兵
       
            for (var x = 0; x < 3; x++){
                var alien = this.aliens.create(x * 200, -20+x*55, 'invader');
                
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien.play('fly');
                alien.body.velocity.y = 45;
            }

            for (var x = 0; x < 5; x++){
                var alien2 = this.aliens.create(x * 100, -300-x*55, 'invader');
                
                alien2.anchor.setTo(0.5, 0.5);
                alien2.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien2.play('fly');
                alien2.body.velocity.y = 45;
            }

            for (var x = 0; x < 2; x++){
                var alien3 = this.aliens.create(x * 300, -750, 'invader');
                
                alien3.anchor.setTo(0.5, 0.5);
                alien3.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien3.play('fly');
                alien3.body.velocity.y = 45;
            }
            for (var x = 0; x < 3; x++){
                var alien4 = this.aliens.create(x * 150, -850, 'invader');
                
                alien4.anchor.setTo(0.5, 0.5);
                alien4.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien4.play('fly');
                alien4.body.velocity.y =45 ;
            }
            for (var x = 0; x < 7; x++){
                var alien5 = this.aliens.create(x * 50, -1000, 'invader');
                
                alien5.anchor.setTo(0.5, 0.5);
                alien5.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien5.play('fly');
                alien5.body.velocity.y = 45;
            }
            //////////////////////////////
            for (var x = 0; x < 5; x++){
                var alien = this.aliens.create(x * 100, -1300+x*55, 'invader');
                
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien.play('fly');
                alien.body.velocity.y = 45;
            }
            for (var x = 0; x < 5; x++){
                var alien2 = this.aliens.create(x * 100, -1300-x*55, 'invader');
                
                alien2.anchor.setTo(0.5, 0.5);
                alien2.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien2.play('fly');
                alien2.body.velocity.y = 45;
            }

            for (var x = 0; x < 7; x++){
                var alien3 = this.aliens.create(200+x * 50, -1750-x*55, 'invader');
                
                alien3.anchor.setTo(0.5, 0.5);
                alien3.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien3.play('fly');
                alien3.body.velocity.y = 45;
            }
            for (var x = 0; x < 7; x++){
                var alien4 = this.aliens.create(200+x * -50, -1750-x*55, 'invader');
                
                alien4.anchor.setTo(0.5, 0.5);
                alien4.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien4.play('fly');
                alien4.body.velocity.y =45 ;
            }
            for (var x = 0; x < 8; x++){
                for (var y = 0; y < 3; y++){
                    var alien5 = this.aliens.create(-100+x * 40, -2500+y*15, 'invader');
                    
                    alien5.anchor.setTo(0.5, 0.5);
                    alien5.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                    alien5.play('fly');
                    alien5.body.velocity.y = 42;

                }
            }
            for (var x = 0; x < 8; x++){
                for (var y = 0; y < 3; y++){
                    var alien5 = this.aliens.create(100+x * 40, -2800+y*15, 'invader');
                    
                    alien5.anchor.setTo(0.5, 0.5);
                    alien5.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                    alien5.play('fly');
                    alien5.body.velocity.y = 42;

                }
            }

        
        this.aliens.x = 100;
       // this.aliens.y = -70;
        //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        var tween = game.add.tween(this.aliens).to( { x: 300 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        //  When the tween loops it calls descend
        //tween.onLoop.add(this.descend, this);

    },

    setupInvader: function(invader) {
        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');
    },

    
    
    collisionHandler:function(bullet, aliens) {
        //  When a bullet hits an alien we kill them both

        bullet.kill();

        aliens.kill();
        //  Increase the score

        this.score += 40;
        this.scoreText.text = this.scoreString + this.score;

        //  And create an explosion :)

        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(aliens.body.x, aliens.body.y);
        explosion.play('kaboom', 30, false, true);
        this.sexplosion.play();
        this.sexplosion.volume=1;
        if (this.aliens.countLiving() == 0){

            this.score += 100;
            this.scoreText.text = this.scoreString + this.score;
            this.enemyBullets.callAll('kill',this);
            //this.stateText.text = " You can challenge more difficult level, \n Click to go to next level";
            //this.stateText.visible = true;

            //the "click to restart" handler
            //game.input.onTap.addOnce(this.nextlevel,this);
        }

    },
    collisionHandlerforX:function(bullet2, aliens) {
        //  When a bullet hits an alien we kill them both

        bullet2.kill();

        aliens.kill();
        //  Increase the score

        this.score += 80;
        this.scoreText.text = this.scoreString + this.score;

        //  And create an explosion :)

        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(aliens.body.x, aliens.body.y);
        explosion.play('kaboom', 30, false, true);
        this.sexplosion.play();
        this.sexplosion.volume=1;
        if (this.aliens.countLiving() == 0){

            this.score += 100;
            this.scoreText.text = this.scoreString + this.score;
            this.enemyBullets.callAll('kill',this);
            //this.stateText.text = " You can challenge more difficult level, \n Click to go to next level";
            //this.stateText.visible = true;

            //the "click to restart" handler
            //game.input.onTap.addOnce(this.nextlevel,this);
        }

    },
    collisionHandlerforaim:function(aimBullet, aliens) {
        //  When a bullet hits an alien we kill them both

        aimBullet.kill();

        aliens.kill();
        //  Increase the score

        this.score += 80;
        this.scoreText.text = this.scoreString + this.score;

        //  And create an explosion :)

        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(aliens.body.x, aliens.body.y);
        explosion.play('kaboom', 30, false, true);
        this.sexplosion.play();
        this.sexplosion.volume=1;
        if (this.aliens.countLiving() == 0){

            this.score += 100;
            this.scoreText.text = this.scoreString + this.score;
            this.enemyBullets.callAll('kill',this);
            //this.stateText.text = " You can challenge more difficult level, \n Click to go to next level";
            //this.stateText.visible = true;

            //the "click to restart" handler
            //game.input.onTap.addOnce(this.nextlevel,this);
        }

    },
///////win to nextlevel
    hitboss1:function(bullet, boss){
        boss.kill();
        this.bosshitnum+=1;
        this.score += 10;
        this.scoreText.text = this.scoreString + this.score;
        if(this.bosshitnum==250){//boss live = 50
            bullet.kill();
            this.bosshitnum=0;
            this.bosskill=true;//
            //  And create an explosion :)
            var explosion = this.explosions.getFirstExists(false);
            explosion.reset(boss.body.x, boss.body.y);
            explosion.play('kaboom', 30, false, true);
            var explosion2 = this.explosions.getFirstExists(false);
            explosion2.reset(boss.body.x+30, boss.body.y);
            explosion2.play('kaboom', 30, false, true);
            var explosion3 = this.explosions.getFirstExists(false);
            explosion3.reset(boss.body.x-30 , boss.body.y);
            explosion3.play('kaboom', 30, false, true);
            this.sexplosion.play();
            this.sexplosion.volume=1;

        }
        if (this.bosskill==true){

            this.score += 2000;
            this.scoreText.text = this.scoreString + this.score;
            this.stateText.text = " You win \n Click to go back to menu ";
            this.stateText.visible = true;

            //the "click to nextlevel" handler
            game.input.onTap.addOnce(this.backmenu,this);
        }
        

        this.emitter.x=this.boss.x;
        this.emitter.y=this.boss.y+10;
        this.emitter.start(true,800,null,15);
        
    },
    hitboss2:function(Xbullet, boss){
        boss.kill();
        this.bosshitnum+=1;
        this.score += 10;
        this.scoreText.text = this.scoreString + this.score;
        if(this.bosshitnum==250){//boss live = 50
            Xbullet.kill();
            this.bosshitnum=0;
            this.bosskill=true;//
            //  And create an explosion :)
            var explosion = this.explosions.getFirstExists(false);
            explosion.reset(boss.body.x, boss.body.y);
            explosion.play('kaboom', 30, false, true);
            var explosion2 = this.explosions.getFirstExists(false);
            explosion2.reset(boss.body.x+30, boss.body.y);
            explosion2.play('kaboom', 30, false, true);
            var explosion3 = this.explosions.getFirstExists(false);
            explosion3.reset(boss.body.x-30 , boss.body.y);
            explosion3.play('kaboom', 30, false, true);
            this.sexplosion.play();
            this.sexplosion.volume=1;
        }
        if (this.bosskill==true){

            this.score += 2000;
            this.scoreText.text = this.scoreString + this.score;
            this.stateText.text = "You win \n Click to go back to menu ";
            this.stateText.visible = true;

            //the "click to nextlevel" handler
            game.input.onTap.addOnce(this.backmenu,this);
        }
        
        this.emitter.x=this.boss.x;
        this.emitter.y=this.boss.y+10;
        this.emitter.start(true,800,null,15);
        
    },
    hitboss3:function(aimbullet, boss){
        boss.kill();
        this.bosshitnum+=1;
        this.score += 10;
        this.scoreText.text = this.scoreString + this.score;
        if(this.bosshitnum==250){//boss live = 250
            aimbullet.kill();
            this.bosshitnum=0;
            this.bosskill=true;//
            //  And create an explosion :)
            var explosion = this.explosions.getFirstExists(false);
            explosion.reset(boss.body.x, boss.body.y);
            explosion.play('kaboom', 30, false, true);
            var explosion2 = this.explosions.getFirstExists(false);
            explosion2.reset(boss.body.x+30, boss.body.y);
            explosion2.play('kaboom', 30, false, true);
            var explosion3 = this.explosions.getFirstExists(false);
            explosion3.reset(boss.body.x-30 , boss.body.y);
            explosion3.play('kaboom', 30, false, true);
            this.sexplosion.play();
            this.sexplosion.volume=1;
        }
        if (this.bosskill==true){

            this.score += 2000;
            this.scoreText.text = this.scoreString + this.score;
            this.stateText.text = " You win \n Click to go back to menu ";
            this.stateText.visible = true;

            //the "click to nextlevel" handler
            game.input.onTap.addOnce(this.backmenu,this);
        }
        
        this.emitter.x=this.boss.x;
        this.emitter.y=this.boss.y+10;
        this.emitter.start(true,800,null,15);
        
    },
    enemyHitsPlayer:function(player,bullet) {
        bullet.kill();
        this.live = this.lives.getFirstAlive();

        if (this.live){
            this.live.kill();
        }
        


        //  And create an explosion :)
        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(player.body.x+30, player.body.y+10);
        explosion.play('kaboom', 30, false, true);
        this.sexplosion.play();
        this.sexplosion.volume=1;
        // When the player dies

        if (this.lives.countLiving() < 1) {

            player.kill();

            this.enemyBullets.callAll('kill');
            this.stateText.text=" GAME OVER \n Click to back to menu ";
            this.stateText.visible = true;
            
        //the "click to restart" handler
        //game.input.onDown.addOnce(this.restart,this);
        game.input.onTap.addOnce(this.backmenu,this);// Click to restart

        }
    },

    BossHitsPlayer:function(player,bossbullet) {
        bossbullet.kill();
        this.live = this.lives.getFirstAlive();

        if (this.live){
            this.live.kill();
        }



        //  And create an explosion :)
        var explosion = this.explosions.getFirstExists(false);
        explosion.reset(player.body.x+30, player.body.y+10);
        explosion.play('kaboom', 30, false, true);

        // When the player dies

        if (this.lives.countLiving() < 1) {

            player.kill();

            this.enemyBullets.callAll('kill');
            this.stateText.text=" GAME OVER \n Click to back to menu ";
            this.stateText.visible = true;
            if(this.RButton.isDown){
                this.restart();
            }
            //RButton.isDown.addOnce(this.restart,this);
        //the "click to restart" handler
        game.input.onTap.addOnce(this.backmenu,this);

        }
    },
    enemyFires: function() {
        //  Grab the first bullet we can from the pool
        this.enemyBullet = this.enemyBullets.getFirstExists(false);
        livingEnemies = [];
        livingEnemies.length=0;
        
        this.aliens.forEachAlive(function(alien){
            // put every living enemy in an array
            livingEnemies.push(alien);

        });

        if (this.enemyBullet && livingEnemies.length > 0){
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);
            // randomly select one of them
            var shooter=livingEnemies[random];

            // And fire the bullet from this enemy
            this.enemyBullet.reset(shooter.body.x, shooter.body.y);
            game.physics.arcade.moveToObject(this.enemyBullet,this.player,175);
            this.firingTimer = game.time.now + 7;//////////////
        }
    },


    bossFires: function() {
        
           game.add.tween(this.bossbullets.scale).to({x:1,y:1.2},500).yoyo(true).start();
            var bossbullet = this.bossbullets.create(this.boss.x-30, this.boss.y +70, 'bossbullet');
            if (bossbullet){
                //  And fire it
                bossbullet.body.velocity.y = 75;
                this.bossbulletTime = game.time.now + 1000;
            }
            var bossbullet2 = this.bossbullets.create(this.boss.x-30, this.boss.y +70, 'bossbullet');
            if (bossbullet2){
                //  And fire it
                bossbullet2.body.velocity.y = 75;
                bossbullet2.body.velocity.x = -75;
                this.bossbulletTime = game.time.now + 1000;
            }
            var bossbullet3 = this.bossbullets.create(this.boss.x-30, this.boss.y +70, 'bossbullet');
            if (bossbullet3){
                //  And fire it
                bossbullet3.body.velocity.y = 75;
                bossbullet3.body.velocity.x = +75;
                this.bossbulletTime = game.time.now + 1000;
            }
            var bossbullet4 = this.bossbullets.create(this.boss.x-30, this.boss.y +70, 'bossbullet');
            if (bossbullet4){
                //  And fire it
                bossbullet4.body.velocity.y = 75;
                bossbullet4.body.velocity.x = 150;
                this.bossbulletTime = game.time.now + 1000;
            }
            var bossbullet5 = this.bossbullets.create(this.boss.x-30, this.boss.y +70, 'bossbullet');
            if (bossbullet5){
                //  And fire it
                bossbullet5.body.velocity.y = 75;
                bossbullet5.body.velocity.x = -150;
                this.bossbulletTime = game.time.now + 1000;
            }
            
        
    },

    fireBullet: function() {
        //  To avoid them being allowed to fire too fast we set a time limit
        
        if (game.time.now >  bulletTime){

            //  Grab the first bullet we can from the pool
            var bullet = this.bullets.getFirstExists(false);
            if (bullet){
                //  And fire it
                bullet.reset(this.player.x, this.player.y -30);
                bullet.body.velocity.y = -400;
                bulletTime = game.time.now + 200;
                
            }
        }
    },

    fireX: function() {
        //  To avoid them being allowed to fire too fast we set a time limit
        
        if (game.time.now >  bulletTimeX){

            //  Grab the first bullet we can from the pool
            var bullet = this.Xbullets.getFirstExists(false);
            if (bullet){
                //  And fire it
                bullet.reset(this.player.x, this.player.y -30);
                bullet.body.velocity.y = -400;
                bulletTimeX = game.time.now + 5;
                
            }
        }
    },
    getaimBullet:function(){
        if (game.time.now >  aimbulletTime){

            //  Grab the first bullet we can from the pool
            this.aimbullet = this.aimbullets.getFirstExists(false);
            if (this.aimbullet){
                //  And fire it
                this.aimbullet.reset(this.player.x-10, this.player.y -30);
                if(this.boss.y>0){
                    game.physics.arcade.moveToObject(this.aimbullet,this.boss,400);  
                }else{
                    game.physics.arcade.moveToObject(this.aimbullet,this.aliens,400);
                }
                //aimbullet.body.velocity.y = 400
                aimbulletTime = game.time.now + 100;
                
            }
        }
    },
/////////////helper
    gethelper:function(){
        this.helper.reset(this.player.x+50, this.player.y );
    },


    helperfireBullet: function() {
        //  To avoid them being allowed to fire too fast we set a time limit
        this.sshoot.play();
        if (game.time.now >  hbulletTime){

            //  Grab the first bullet we can from the pool
            var hbullet = this.bullets.getFirstExists(false);
            if (hbullet){
                //  And fire it
                hbullet.reset(this.player.x+40, this.player.y -30);
                hbullet.body.velocity.y = -300;
                hbulletTime = game.time.now + 200;
                
                
            }
        }
    },
////////////

    resetBullet: function(bullet,bullet2,bossbullet,aimbullet) {
        //  Called if the bullet goes out of the screen
        bullet.kill();
        bullet2.kill();
        bossbullet.kill();
        aimbullet.kill();
    },

    restart: function() {
        //  A new level starts
        //resets the life count
        this.lives.callAll('revive');
        this.boss.revive();
        this.boss.y=-4000;
        this.bossbullets.removeAll();

        //  And brings the aliens back from the dead :)
        this.aliens.removeAll();
        this.createAliens();
        this.PowerRings.removeAll();
        this.createPowerRings();
        //revives the player
        this.player.revive();
        //hides the text
        this.stateText.visible = false;
    },
    nextlevel: function() {
        
         game.state.start('main2');
         this.backmusic.volume=0;
     },
    backmenu: function() {
        
        game.state.start('menu');
        this.backmusic.volume=0;
    },
    unpause:function(){
        game.paused = false;
    }
};



