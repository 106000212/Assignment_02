var controlState ={
    preload:function(){
        
    },
    create:function(){
        game.add.image(0,0,'cbackground');
        var nLabel = game.add.text(game.width/2,60,'操作說明',{font:'50px Arial',fill:'#ffffff'});
        nLabel.anchor.setTo(0.5,0.5);
        var fLabel = game.add.text(game.width/2,150,'用上下左右鍵移動player，空白鍵發射子彈',{font:'25px Arial',fill:'#ffffff'});
        fLabel.anchor.setTo(0.5,0.5);

        var sLabel = game.add.text(game.width/2,250,'吃到ring集到5格burst計量後長按X鍵發動必殺技',{font:'25px Arial',fill:'#ffffff'});
        sLabel.anchor.setTo(0.5,0.5);

        var ffLabel = game.add.text(game.width/2,350,'音量控制按A或D鍵可以調高或低音量，按Q鍵可以暫停/回復遊戲',{font:'25px Arial',fill:'#ffffff'});
        ffLabel.anchor.setTo(0.5,0.5);

        var tLabel = game.add.text(game.width/2,450,'吃到一定數量ring可發出追蹤敵人子彈和小幫手',{font:'25px Arial',fill:'#ffffff'});
        tLabel.anchor.setTo(0.5,0.5);

        var startLabel = game.add.text(game.width/2,550,'please press SPACEBAR key to start',{font:'35px Arial',fill:'#ffffff'});
        startLabel.anchor.setTo(0.5,0.5);

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        upKey.onDown.add(this.start,this);
    },
    start:function(){
        game.state.start('main');
    },
};